import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import { Nav, Navbar, Form, FormControl, Button, Card } from "react-bootstrap";

function Home() {
  let arr = [1, 2, 3, 4, 5, 6];
  let data = arr.map((x) => (
    <div className="col-4">
      <Card className="mt-3">
        <Card.Img
          variant="top"
          src="http://img.youtube.com/vi/63z9YDDIM0g/maxresdefault.jpg"
        />
        <Card.Body>
          <Card.Title>Don't let me go</Card.Title>
          <Card.Text>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Explicabo cumque neque debitis.{" "}
          </Card.Text>
          <Link className="btn btn-outline-primary" to={`/posts/${x}`}> See more</Link>
       
        </Card.Body>
        <Card.Footer>
          <small className="text-muted">Last updated 2 years ago</small>
        </Card.Footer>
      </Card>
    </div>
  ));
  return (
    <div
      className="container-fluid"
      style={{ paddingLeft: "8%", paddingRight: "8%" }}
    >
      <div className="row">{data}</div>
    </div>
  );
}

export default Home;
