import React from "react";

function ReactRouter() {
  return (
    <div
      className="container-fluid"
      style={{ paddingLeft: "8%", paddingRight: "8%" }}
    >
      <div className="box rounded mt-4 bg-light p-5 h-75">
      <h1 className="label-info h1" >This is main page for you !</h1>
      </div>
    </div>
  );
}

export default ReactRouter;
