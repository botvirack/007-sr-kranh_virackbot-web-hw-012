import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import queryString from "query-string";

function Account(props) {
  let acc = queryString.parse(props.location.search);

  return (
    <div
      className="container-fluid pl-5 pr-2"
    >
      <h1>Account</h1>
      <ul>
        <li>
          {" "}
          <Link as={Link} to="/account?name=Virackbot">
            Virackbot
          </Link>
        </li>
        <li>
          {" "}
          <Link as={Link} to="/account?name=Panha">
            Panha
          </Link>
        </li>
        <li>
          {" "}
          <Link as={Link} to="/account?name=Chiva">
            Chiva
          </Link>
        </li>

        <li>
          {" "}
          <Link as={Link} to="/account?name=Sopheap">
          Sopheap
          </Link>
        </li>
        <li>
          {" "}
          <Link as={Link} to="/account?name=Ratanak">
          Ratanak
          </Link>
        </li>
      </ul>
      {acc.name === undefined ? (
        <h1>There is no name in the querystring</h1>
      ) : (
        <h1>
          The <span style={{ color: "green" }}>name</span> in the query string is{" "}
          {`"${acc.name}"`}
        </h1>
      )}
    </div>
  );
}

export default Account;
