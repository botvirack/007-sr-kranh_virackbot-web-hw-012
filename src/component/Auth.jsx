import React from "react";
import App from "../App";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
} from "react-router-dom";
import Welcome from "./Welcome";

function Auth(props) {
  return (
    <div
      className="container-fluid"
      style={{ paddingLeft: "8%", paddingRight: "8%" }}
    >
      <div className="mt-5">

   
   <form className="col-3 mx-auto my-auto">
     <label className="label">Username</label>
      <input className="form-control" type="text" placeholder="Username" />
      <label className="label">Password</label>
      <input  className="form-control" type="password" placeholder="Password" />

      <Link as={Link} to="/welcome">
        <button className="btn btn-primary mt-3" onClick={props.changeAuth}>Submit</button>
      </Link>
      </form>
      </div>
    </div>

    
    
  );
}

export default Auth;
